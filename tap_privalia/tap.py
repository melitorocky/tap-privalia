"""privalia tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_privalia.streams import (
    OrdersStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    OrdersStream
]


class Tapprivalia(Tap):
    """privalia tap class."""
    name = "tap-privalia"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property("apiUrlPinkConnect", th.StringType, required=True),
        th.Property("apiTokenPinkConnect", th.StringType, required=True),
        th.Property("apiUrlIzberg", th.StringType, required=True),
        th.Property("apiKeyIzberg", th.StringType, required=False),
        th.Property("apiTokenIzberg", th.StringType, required=True),
        th.Property("merchantId", th.StringType, required=False),
        th.Property("maxNumOrders", th.NumberType, required=True),
        th.Property("start_date", th.DateTimeType, required=False),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
