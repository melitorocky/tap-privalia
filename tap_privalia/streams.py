"""Stream type classes for tap-privalia."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_privalia.client import privaliaStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class OrdersStream(privaliaStream):
    """Define custom stream."""
    name = "orders"
    path = "/orders"
    primary_keys = ["orderId"]
    replication_key = "updateOrderDate"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        # Order - PinkConnect
        th.Property("orderId", th.IntegerType),
        th.Property("marketplaceName", th.StringType),
        th.Property("marketplaceCode", th.StringType),
        th.Property("marketplaceOrderCode", th.StringType),
        th.Property("shopChannelId", th.IntegerType),
        th.Property("shopChannelName", th.StringType),
        th.Property("status", th.StringType),
        th.Property("createOrderDate", th.DateTimeType),
        th.Property("updateOrderDate", th.DateTimeType),
        th.Property("shippedOrderDate", th.DateTimeType),
        th.Property("totalPrice", th.NumberType),
        th.Property("shippingCosts", th.NumberType),
        th.Property("shippingTaxRate", th.NumberType),
        th.Property("currency", th.StringType),
        th.Property("deliveryNote", th.StringType),
        th.Property("shippingInformation",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("address", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("zipCode", th.StringType),
                th.Property("country", th.StringType),
                th.Property("countryIsoCode", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("comment", th.StringType),
            )
        ),
        th.Property("pickupPointId", th.StringType),
        th.Property("billingInformation",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("address", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("zipCode", th.StringType),
                th.Property("country", th.StringType),
                th.Property("countryIsoCode", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("comment", th.StringType),
            ),
        ),
        th.Property("deliveryDetails",
            th.ArrayType(
                th.ObjectType(
                    th.Property("carrierId", th.IntegerType),
                    th.Property("carrierName", th.StringType),
                    th.Property("trackingNumber", th.StringType),
                    th.Property("trackingUrl", th.StringType),
                )
            )
        ),
        th.Property("orderLines",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("gtin", th.StringType),
                    th.Property("sku", th.StringType),
                    th.Property("name", th.StringType),
                    th.Property("price", th.NumberType),
                    th.Property("taxRate", th.NumberType),
                    th.Property("quantity", th.IntegerType),
                    th.Property("brandName", th.StringType),
                    th.Property("totalPrice", th.NumberType),
                    th.Property("manufacturerReference", th.StringType),
                    th.Property("status",
                        th.ObjectType(
                            th.Property("in_stock", th.IntegerType),
                            th.Property("out_of_stock", th.IntegerType),
                            th.Property("shipped", th.IntegerType),
                            th.Property("returned", th.IntegerType),
                            th.Property("cancelled", th.IntegerType),
                            th.Property("processing", th.IntegerType),
                            th.Property("pending", th.IntegerType),
                        )
                    )
                )
            ),
        ),
        th.Property("additionalInformation",
            th.ObjectType(
                th.Property("izberg_id", th.StringType),
            ),
        ),
        th.Property("deliveryDetails",
            th.ArrayType(
                th.ObjectType(
                    th.Property("date", th.DateTimeType),
                    th.Property("productCost", th.NumberType),
                    th.Property("shippingCost", th.NumberType),
                    th.Property("currency", th.StringType),
                    th.Property("type", th.StringType),
                )
            ),
        ),
        th.Property("refunds",
            th.ArrayType(
                th.ObjectType(
                    th.Property("date", th.DateTimeType),
                    th.Property("productCost", th.NumberType),
                    th.Property("shippingCost", th.NumberType),
                    th.Property("currency", th.StringType),
                    th.Property("type", th.StringType),
                )
            )
        ),
        th.Property("requestedShippingMethod", th.StringType),
        th.Property("memberId", th.StringType),


        # Izberg data
        th.Property("izbergFields",
            th.ObjectType(
                th.Property("amount", th.NumberType),
                th.Property("amount_vat_included", th.NumberType),
                th.Property("amount_vat_included_before_discount", th.NumberType),
                th.Property("app_discount_amount", th.NumberType),
                th.Property("app_discount_amount_on_items_vat_included", th.NumberType),
                th.Property("app_discount_amount_on_shipping_vat_included", th.NumberType),
                th.Property("app_discount_amount_vat_included", th.NumberType),
                th.Property("application",
                    th.ObjectType(
                        th.Property("id", th.NumberType),
                        th.Property("name", th.StringType),
                        th.Property("namespace", th.StringType),
                        th.Property("pk", th.NumberType),
                        th.Property("resource_uri", th.StringType),
                    )
                ),
                th.Property("attributes",
                    th.ObjectType(
                        th.Property("empty_holder", th.NumberType), # Fake field for BigQuery Record (cannot be null)
                    )
                ),
                th.Property("auto_cancelling_date", th.DateTimeType),
                th.Property("billing_address",
                    th.ObjectType(
                        th.Property("address", th.StringType),
                        th.Property("address2", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("country",
                            th.ObjectType(
                                th.Property("code", th.StringType),
                                th.Property("id", th.NumberType),
                                th.Property("name", th.StringType),
                            )
                        ),
                        th.Property("first_name", th.StringType),
                        th.Property("fiscal_number", th.StringType),
                        th.Property("id", th.NumberType),
                        th.Property("last_name", th.StringType),
                        th.Property("name", th.StringType),
                        th.Property("phone", th.StringType),
                        th.Property("resource_uri", th.StringType),
                        th.Property("state", th.StringType),
                        th.Property("status", th.StringType),
                        th.Property("zipcode", th.StringType),
                    )
                ),
                th.Property("confirmation_date", th.DateTimeType),
                th.Property("created_on", th.DateTimeType),
                th.Property("credit_notes",
                    th.ArrayType(
                        th.StringType
                    )
                ),
                th.Property("currency",
                    th.ObjectType(
                        th.Property("code", th.StringType),
                        th.Property("pk", th.StringType),
                        th.Property("resource_uri", th.StringType),
                    )
                ),
                th.Property("customer_invoices",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("id", th.IntegerType),
                            th.Property("pk", th.IntegerType),
                            th.Property("resource_uri", th.StringType),
                        )
                    )
                ),
                th.Property("debug", th.BooleanType),
                th.Property("discount_amount", th.NumberType),
                th.Property("discount_amount_vat_included", th.NumberType),
                th.Property("eco_tax", th.NumberType),
                th.Property("eco_tax_vat_included", th.NumberType),
                th.Property("entered_discount_codes",
                    th.ArrayType(
                        th.StringType
                    )
                ),
                th.Property("external_id", th.StringType),  # INT?
                th.Property("external_manager", th.StringType),
                th.Property("external_manager_id", th.StringType),  # INT?
                th.Property("external_manager_status", th.StringType),
                th.Property("extra_data", th.StringType),
                th.Property("id", th.NumberType),
                th.Property("incident_status", th.StringType),
                th.Property("items",
                    th.ArrayType(
                        th.ObjectType(
                            th.Property("amount", th.NumberType),
                            th.Property("amount_vat_included", th.NumberType),
                            th.Property("bundled", th.BooleanType),
                            th.Property("color", th.StringType),
                            th.Property("currency",
                                th.ObjectType(
                                    th.Property("code", th.StringType),
                                    th.Property("pk", th.StringType),
                                    th.Property("resource_uri", th.StringType),
                                )
                            ),
                            th.Property("discount_amount_financed_by_application_with_tax", th.NumberType),
                            th.Property("discount_amount_vat_included", th.NumberType),
                            th.Property("discounts",
                                th.ArrayType(
                                    th.StringType
                                )
                            ),
                            th.Property("external_id", th.StringType),
                            th.Property("gift", th.StringType),
                            th.Property("gift", th.StringType),
                            th.Property("id", th.NumberType),
                            th.Property("invoiceable_quantity", th.NumberType),
                            th.Property("invoiced_quantity", th.NumberType),
                            th.Property("item_image_url", th.StringType),
                            th.Property("item_type", th.StringType),
                            th.Property("max_invoiceable", th.NumberType),
                            th.Property("name", th.StringType),
                            th.Property("offer_absolute_url", th.StringType),
                            th.Property("offer_external_id", th.StringType),  # INT?
                            th.Property("offer_id", th.NumberType),
                            th.Property("previous_price", th.NumberType),
                            th.Property("previous_price_without_vat", th.NumberType),
                            th.Property("price", th.NumberType),
                            th.Property("product",
                                th.ObjectType(
                                    th.Property("id", th.NumberType),
                                    th.Property("pk", th.NumberType),
                                    th.Property("resource_uri", th.StringType),
                                )
                            ),
                            th.Property("product_id", th.NumberType),
                            th.Property("product_offer",
                                th.ObjectType(
                                    th.Property("id", th.NumberType),
                                    th.Property("pk", th.NumberType),
                                    th.Property("resource_uri", th.StringType),
                                    th.Property("stock", th.NumberType),
                                )
                            ),
                            th.Property("quantity", th.NumberType),
                            th.Property("resource_uri", th.StringType),
                            th.Property("shipping", th.NumberType),
                            th.Property("shipping_speed", th.StringType),
                            th.Property("size", th.StringType),
                            th.Property("sku", th.StringType),
                            th.Property("status", th.StringType),
                            th.Property("status_localized", th.StringType),
                            th.Property("variation", th.StringType),
                            th.Property("variation_external_id", th.StringType),  # INT?
                            th.Property("variation_kind",
                                th.ArrayType(
                                    th.StringType
                                )
                            ),
                            th.Property("variation_name", th.StringType),
                            th.Property("variation_sku", th.StringType),
                            th.Property("variation_stock", th.NumberType),
                            th.Property("vat", th.NumberType),
                        )
                    )
                ),
                th.Property("items_count", th.NumberType),
                th.Property("last_modified", th.DateTimeType),
                th.Property("merchant",
                    th.ObjectType(
                        th.Property("id", th.NumberType),
                        th.Property("name", th.StringType),
                        th.Property("resource_uri", th.StringType),
                        th.Property("slug", th.StringType),
                    )
                ),
                th.Property("merchant_comment", th.StringType),
                th.Property("meta",
                    th.ObjectType(
                        th.Property("empty_holder", th.NumberType), # Fake field for BigQuery Record (cannot be null)
                    )
                ),
                th.Property("order",
                    th.ObjectType(
                        th.Property("cart_id", th.NumberType),
                        th.Property("created_on", th.DateTimeType),
                        th.Property("external_id", th.StringType),
                        th.Property("id", th.NumberType),
                        th.Property("id_number", th.StringType),
                        th.Property("last_updated", th.DateTimeType),
                        th.Property("resource_uri", th.StringType),
                        th.Property("shipping_speed", th.StringType),
                        th.Property("status", th.StringType),
                        th.Property("status_localized", th.StringType),
                    )
                ),
                th.Property("payment_type", th.StringType),
                th.Property("price", th.NumberType),
                th.Property("price_vat_included", th.NumberType),
                th.Property("readonly", th.BooleanType),
                th.Property("received", th.BooleanType),  # Should be Boolean, but it's always NULL..
                th.Property("refunded_amount", th.NumberType),
                th.Property("resource_uri", th.StringType),
                th.Property("shipping", th.NumberType),
                th.Property("shipping_address",
                    th.ObjectType(
                        th.Property("address", th.StringType),
                        th.Property("address2", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("country",
                            th.ObjectType(
                                th.Property("code", th.StringType),
                                th.Property("id", th.NumberType),
                                th.Property("name", th.StringType),
                            )
                        ),
                        th.Property("first_name", th.StringType),
                        th.Property("fiscal_number", th.StringType),
                        th.Property("id", th.NumberType),
                        th.Property("last_name", th.StringType),
                        th.Property("name", th.StringType),
                        th.Property("phone", th.StringType),
                        th.Property("resource_uri", th.StringType),
                        th.Property("state", th.StringType),
                        th.Property("status", th.StringType),
                        th.Property("zipcode", th.StringType),
                    )
                ),
                th.Property("shipping_vat_included", th.NumberType),
                th.Property("starred", th.BooleanType),
                th.Property("status", th.StringType),
                th.Property("status_localized", th.StringType),
                th.Property("user",
                    th.ObjectType(
                        th.Property("display_name", th.StringType),
                        th.Property("email", th.StringType),
                        th.Property("first_name", th.StringType),
                        th.Property("id", th.NumberType),
                        th.Property("last_name", th.StringType),
                        th.Property("resource_uri", th.StringType),
                        th.Property("username", th.StringType),
                    )
                ),
                th.Property("vat", th.NumberType),
                th.Property("vat_collected_by_merchant", th.NumberType),
                th.Property("vat_collected_by_operator", th.NumberType),
                th.Property("vat_on_eco_tax", th.NumberType),
                th.Property("vat_on_products", th.NumberType),
                th.Property("vat_on_shipping", th.NumberType),
                th.Property("vat_rate_key_on_shipping", th.StringType),
                th.Property("vat_rate_on_shipping", th.NumberType),
                th.Property("visible_merchant", th.BooleanType),
                th.Property("workflow", th.StringType),
            ),
        ),
    ).to_dict()
