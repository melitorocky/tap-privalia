import requests, json, urllib
from requests.auth import HTTPBasicAuth

class Orders:
    basePathPinkConnect = ''
    basePathIzberg = ''
    accessTokenPinkConnect = ''
    accessTokenIzberg = ''
    merchantId = ''


    def __init__(self, credentials=None):
        self.basePathPinkConnect = credentials['apiUrlPinkConnect']
        self.basePathIzberg = credentials['apiUrlIzberg']
        self.accessTokenPinkConnect = credentials['apiTokenPinkConnect']
        self.accessTokenIzberg = credentials['apiTokenIzberg']
        # self.merchantId = credentials['merchantId']
        self.maxNumOrders = credentials['maxNumOrders']
        self.start_date = credentials['start_date']
    

    def get_orders(self, **kwargs):
        dictOrders = {}

        endpoint = 'https://' + self.basePathPinkConnect + '/orders'
        headers = {
            # 'content-type'  :   'application/x-www-form-urlencoded',
            'Authorization' :   'Bearer ' + self.accessTokenPinkConnect
        }

        response = requests.get(
            endpoint,
            headers=headers,
            params=kwargs
        )

        # Check if positive response
        if response.status_code == 200:
            dictOrders = json.loads(response.text)
        else:
            print("OH NO, NON ACCEDO AGLI ORDINI!")
            print(f"Codice: {response.status_code} - Errore: {response.text}")
            quit()

        return dictOrders
    

    def get_order_additional_info(self, **kwargs):
        dictOrderInfos = {}

        endpoint = 'https://' + self.basePathIzberg + '/v1/merchant_order/' + kwargs['izberg_id']
        headers = {
            'Authorization' :   'Bearer ' + self.accessTokenIzberg
        }

        response = requests.get(
            endpoint,
            headers=headers,
            params=kwargs
        )

        # Check if positive response
        if response.status_code == 200:
            dictOrderInfos = json.loads(response.text)
        else:
            print("OH NO, NON ACCEDO ALLE INFO DELL'ORDINE!")
            print(f"Codice: {response.status_code} - Errore: {response.text}")
            quit()

        return dictOrderInfos

