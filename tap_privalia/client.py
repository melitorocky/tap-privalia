"""Custom client handling, including privaliaStream base class."""

import requests,json, psycopg2
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable, cast

from .class_orders import Orders

from singer_sdk.streams import Stream

from datetime import datetime
from datetime import timedelta
from dateutil import parser
import pendulum

import pprint
pp = pprint.PrettyPrinter(indent=2)

import logging
LOGGER = logging.getLogger(__name__)

class privaliaStream(Stream):
    """Stream class for Privalia streams."""

    # State fix
    is_sorted = True

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        streamName = type(self).__name__

        # map the inputs to the function blocks
        if streamName == 'OrdersStream':
            return self.OrdersStream(context)
        else:
            print(streamName + ' not found..')
            quit()


    def get_credentials(self):
        credentials = {
            'apiUrlPinkConnect': self.config["apiUrlPinkConnect"],
            'apiTokenPinkConnect': self.config["apiTokenPinkConnect"],
            'apiUrlIzberg': self.config["apiUrlIzberg"],
            'apiKeyIzberg': self.config["apiKeyIzberg"],
            'apiTokenIzberg': self.config["apiTokenIzberg"],
            # 'merchantId': self.config["merchantId"],
            'maxNumOrders': self.config["maxNumOrders"],
            'start_date': self.config["start_date"],
        }
        return credentials


    def OrdersStream(self,context):
        finalRes = []

        credentials = self.get_credentials()
        last_updated_after = self.getLastReplicationKey(context)
        if not last_updated_after:
            last_updated_after = credentials['start_date']

        # last_updated_after = "2020-12-15T14:31:42"

        OrderClass = Orders(credentials=credentials)

        maxNumOrders = credentials['maxNumOrders']

        # getOrders
        listOrders = OrderClass.get_orders(
            updateDateGTE = last_updated_after,
            limit = maxNumOrders
        )

        for order in listOrders:
            orderFull = order

            # Izberg additional info -- not every order has Izberg stuff connected!!
            if "additionalInformation" in order \
            and isinstance(order["additionalInformation"], dict) \
            and "izberg_id" in order["additionalInformation"]:
                orderFull["izbergFields"] = OrderClass.get_order_additional_info(
                    izberg_id = order["additionalInformation"]["izberg_id"]
                )

                # # Happy testing, BiggoQuero is FUN to work with!
                # del orderFull["refunds"]
                # del orderFull["izbergFields"]["items"]
                # del orderFull["izbergFields"]["credit_notes"]
                # del orderFull["izbergFields"]["entered_discount_codes"]

                # Fix abysmal fields management
                for orderItem in orderFull["izbergFields"]["items"]:
                    # Sometimes False worries BigQ. None is a much better case
                    if not orderItem["gift"]:
                        orderItem["gift"] = None

            # Fix incompatible BigQuery column names
            if "orderLines" in orderFull:
                for orderLine in orderFull["orderLines"]:
                    if "status" in orderLine:
                        # Sometimes status is an empty list rather than a dictionary.
                        if not orderLine["status"]:
                            orderLine["status"] = {"returned": 0} # NOTE: picking up a random key and set it to 0
                        # Dict
                        else:
                            fixedStatus = {}
                            for key,val in orderLine["status"].items():
                                # Sometimes In Stock is "True"
                                if isinstance(val, bool):
                                    val = int(val == True)
                                fixedStatus[key.replace(" ", "_").lower()] = val
                            orderLine["status"] = fixedStatus

            # # Log any key that seems empty ([])
            # if orderFull["izbergFields"]["credit_notes"]:
            #     LOGGER.warning("------------------------------------------")
            #     LOGGER.warning(type(orderFull["izbergFields"]["credit_notes"]))
            #     LOGGER.warning(orderFull["izbergFields"]["credit_notes"])
            #     LOGGER.warning("------------------------------------------")

            # # Log any key that seems empty ({})

            finalRes.append(orderFull)


            # print("\n\n----------------------------\n\n")
            # pprint.pprint(orderFull)
            # print("\n\n----------------------------\n\n")
            # quit()

        # print("ORDINI: ")

        # print("\n\n----------------------------\n\n")
        # pp.pprint(finalRes)
        # print("\n\n----------------------------\n\n")
        # quit()

        return finalRes


    def getLastReplicationKey(self,context):
        replication_key = self.get_starting_timestamp(context)
        pendulum_start_date = cast(datetime, pendulum.parse(self.config["start_date"]))

        if replication_key == pendulum_start_date:
            jobId = self.config["jobId"]
            bookmark = self.config["bookmark"]
            replication_key = self.getLastSuccessfulRunReplicationKey(jobId=jobId, bookmark=bookmark, start_date=replication_key)

        #trasformazioni su start_date: aggiungo un secondo, trasformo in isoformat
        if replication_key:
            replication_key = replication_key + timedelta(seconds=1)
            replication_key = replication_key.to_datetime_string()
            replication_key = parser.parse(replication_key[:19])
            replication_key = replication_key.isoformat()

        return replication_key

    def keys_exists(self, element, *keys):
        '''Check if *keys (nested) exists in `element` (dict).'''
        if not isinstance(element, dict):
            raise AttributeError('keys_exists() expects dict as first argument.')
        if len(keys) == 0:
            raise AttributeError('keys_exists() expects at least two arguments, one given.')

        _element = element
        for key in keys:
            try:
                _element = _element[key]
            except KeyError:
                return False
        return True

    def getLastSuccessfulRunReplicationKey(self, jobId, bookmark, start_date):
        replication_key = start_date
        jsonPayload = '{}'

        conn = None
        try:
            # DB Connection
            conn = psycopg2.connect(
                host=self.config["postgres_remote_host"],
                database=self.config["postgres_remote_database"],
                user=self.config["postgres_remote_user"],
                password=self.config["postgres_remote_password"]
            )

            # Create a cursor
            cur = conn.cursor()

            # Define the query
            query = f"""SELECT payload
                FROM runs
                WHERE
                job_name = '{jobId}' AND
                state = 'SUCCESS'
                ORDER BY id DESC
                LIMIT 1"""

            # Execute the query
            cur.execute(query)

            # Fetch the first row
            queryResult = cur.fetchone()

            if queryResult:
                jsonPayload = queryResult[0]

            # Close the communication with the PostgreSQL
            cur.close()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
            quit()
        finally:
            if conn is not None:
                conn.close()
                # print('Database connection closed.')

        dictPayload = json.loads(jsonPayload)

        if self.keys_exists(dictPayload, "singer_state", "bookmarks", bookmark, "replication_key_value"):
            replication_key = dictPayload['singer_state']['bookmarks'][bookmark]['replication_key_value']
            replication_key = cast(datetime, pendulum.parse(replication_key))

        return replication_key
